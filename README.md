# 01-flask-base-project

Mattia Savoia

Link al progetto di partenza: https://gitlab.com/cicciodev/cloudedgecomputing/-/tree/01-flask-base-project

## Esecuzione in fase di sviluppo
Il seguente comando esegue l'applicazione in fase di sviluppo:
```shell
sh docker_start.sh dev
```

## Esecuzione in fase di produzione
Il seguente comando esegue l'applicazione in fase di produzione:
```shell
sh docker_start.sh prod
```

## Deploy
L'applicazione è disponibile al seguente indirizzo IP pubblico: http://3.252.193.56
