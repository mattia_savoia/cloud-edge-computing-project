#!/bin/sh

if [ "$DB" = "postgresql" ]
then
  echo "Waiting for postgres..."

  while ! nc -z $DB_HOST $DB_PORT; do
    sleep 0.1
  done

  echo "PostgreSQL started!"
fi

# Creazione del DB se non esiste
python manage.py create_db

exec "$@"
