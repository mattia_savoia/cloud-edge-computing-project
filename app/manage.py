from flask.cli import FlaskGroup

from app import app, db


cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    print("Creating database...")
    db.create_all()
    db.session.commit()
    print("Done!")


@cli.command("drop_all_db")
def create_db():
    print("Dropping all database tables...")
    db.drop_all()
    db.create_all()
    db.session.commit()
    print("Done!")


if __name__ == "__main__":
    cli()
