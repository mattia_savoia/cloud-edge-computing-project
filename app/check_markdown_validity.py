import sys
import datetime
import os


markdown_tags = []

BLOG_IMAGES_DIR = "./static/assets/blog-images"
MARKDOWN_TEMPLATE_FILE_PATH = "./posts/template.md.tmpl"
POSTS_PATH = "./posts/"

# campo finale
TAG_THREE_DASH = "---"

# campi opzionali del template
TAG_IMAGE = "image"
TAG_AUTHOR_IMAGE = "author_image"


def markdownTemplateExist():
    # Controllo esistenza file template markdown
    if not os.path.exists(MARKDOWN_TEMPLATE_FILE_PATH):
        return False
    return True


def setMarkdownTags():

    with open(MARKDOWN_TEMPLATE_FILE_PATH, "r") as f:
        for line in f.readlines():
            line = line.strip().lower()

            if line == TAG_THREE_DASH:
                markdown_tags.append(line)
            elif len(line) == 0 or ":" not in line:
                continue
            else:
                markdown_tags.append(line.split(":")[0])

    f.close()
    # print(markdown_tags)


def checkPostValidity(post_file_name):
    # print(post_file_name)

    post_tag_content = {}

    # Popolo il dizionario tag e content del post
    with open(post_file_name, "r") as f:
        for line in f.readlines():
            line = line.strip().lower()

            if line == TAG_THREE_DASH:
                post_tag_content[line] = ""
            elif len(line) == 0 or ":" not in line:
                continue
            else:
                tag, content = line.split(":", 1)
                post_tag_content[tag] = content

    for template_tag in markdown_tags:
        # controllo che siano presenti tutti i tag necessari
        if template_tag not in post_tag_content.keys():
            # questi campi sono opzionali
            if template_tag == TAG_IMAGE or template_tag == TAG_AUTHOR_IMAGE:
                continue
            else:
                print("ERROR: " + template_tag + " tag is required!")
                return False
        else:
            # controllo che ogni valore dei tag non sia vuoto
            if len(post_tag_content[template_tag]) == 0:
                if template_tag == TAG_THREE_DASH:
                    continue
                # questi campi sono opzionali
                elif template_tag == TAG_IMAGE or template_tag == TAG_AUTHOR_IMAGE:
                    print("ERROR: " + template_tag + " content is empty! Optional parameter must not be inserted in the file!")
                    return False
                else:
                    print("ERROR: " + template_tag + " content is empty!")
                    return False
            else:
                # questi campi sono opzionali
                if template_tag == TAG_IMAGE:
                    # Controllo esistenza immagine generale nel path
                    if not os.path.exists(os.path.join(BLOG_IMAGES_DIR, post_tag_content[TAG_IMAGE].strip())):
                        print("ERROR: path to author image does not exist!")
                        return False
                elif  template_tag == TAG_AUTHOR_IMAGE:
                    # Controllo esistenza immagine autore nel path
                    if not os.path.exists(os.path.join(BLOG_IMAGES_DIR, post_tag_content[TAG_AUTHOR_IMAGE].strip())):
                        print("ERROR: path to image does not exist!")
                        return False

    try:
        datetime.datetime.strptime(post_tag_content["date"].strip(), "%B %d, %Y")
    except ValueError:
        print("ERROR: the data format is incorrect! Must be: Month Day, Year!")
        return False

    return True


def main():
        
    if markdownTemplateExist():

        setMarkdownTags()

        for dir in os.listdir(POSTS_PATH):
            if os.path.isdir(POSTS_PATH + dir):
                for filename in os.listdir(POSTS_PATH + dir):
                    if filename.endswith(".md"):
                        post_file_name = POSTS_PATH + dir + "/" + filename
                        if checkPostValidity(post_file_name):
                            print(post_file_name + " is valid!")
                        else:
                            print("ERROR: " + post_file_name + " is not valid!")
                            exit(1)

    else:
        print("ERROR: markdown template does not exist! Please create new template.md.tmpl file!")
        exit(1)


if __name__ == '__main__':
    main()

