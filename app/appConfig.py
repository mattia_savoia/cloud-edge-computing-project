import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = (os.environ.get("DB")+"://"+os.environ.get("POSTGRES_USER")+":"+os.environ.get("POSTGRES_PASSWORD")+"@"+os.environ.get("DB_HOST")+":"+os.environ.get("DB_PORT")+"/"+os.environ.get("POSTGRES_DB"))


class debugConfig(object):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = (os.environ.get("DB")+"://"+os.environ.get("POSTGRES_USER")+":"+os.environ.get("POSTGRES_PASSWORD")+"@"+os.environ.get("DB_HOST")+":"+os.environ.get("DB_PORT")+"/"+os.environ.get("POSTGRES_DB"))
