
ENV_FILE=.env

checkEnvFile(){

    if ! [ -f "$ENV_FILE" ]; then

        echo "$ENV_FILE"" file does not exist! Creating new one..."
        sh init_env_vars.sh
        echo "Done!"

    fi

}

if [ "$1" = "dev" ] || [ "$1" = "DEV" ]; then

    echo "Starting docker in development mode..."

    checkEnvFile

    # Se non è stato eseguito il build, viene eseguito in automatico.
    docker compose -f docker-compose-dev.yml up -d

elif [ "$1" = "prod" ] || [ "$1" = "PROD" ]; then

    echo "Starting docker in production mode..."
    
    checkEnvFile

    # Aggiornamento dell'immagine
    docker compose pull
    docker compose -f docker-compose.yml up -d

else

    echo "ERROR: you must start script with one argument!"
    echo "Possible arguments: dev , DEV , prod , PROD ."
    exit 1

fi
