echo "Setting up environment variables..."

ENV_FILE=.env

if [ -f "$ENV_FILE" ]; then

    echo "$ENV_FILE"" already file exist!"
    mv -f "$ENV_FILE" "$ENV_FILE".$(date -u +%Y%m%d)
    echo "New" "$ENV_FILE" "has been created! The old file has been renamed!"

fi


POSTGRES_USER=flask-db-prod
POSTGRES_PASSWORD=$(openssl rand -base64 32)


cat > "$ENV_FILE" <<EOF

APP_CONTAINER_NAME=flask-app-container
DATABASE_CONTAINER_NAME=database-container
REVERSE_PROXY_CONTAINER_NAME=nginx-container

FLASK_APP=app.py

DB=postgresql
DB_HOST=database
DB_PORT=5432

POSTGRES_USER=${POSTGRES_USER}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
POSTGRES_DB=blog

EOF
